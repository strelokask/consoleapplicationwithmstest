using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplicationWithMSTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Game game = new Game();

            while (game.HasAliveCell())
            {
                DrawBoard(game.Board);

                game.NextStep();
            }
            Console.WriteLine("The game is over!!!");
            Console.WriteLine("Press any to exit");
            Console.ReadKey();
        }
        #region Private Methods

        private static void DrawBoard(int[,] board)
        {
            for (int j = 0; j < board.GetLength(1); j++)
            {
                Console.Write('-');
            }
            
            Console.WriteLine();

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    var ch = board[i, j] == 1 ? '*' : ' ';
                    Console.Write(ch);
                }
                Console.WriteLine();
            }

            for (int j = 0; j < board.GetLength(1); j++)
            {
                Console.Write('-');
            }

            Console.WriteLine();

            Thread.Sleep(2000);

            Console.Out.Flush();
            Console.Clear();
        }
        #endregion
    }
}
