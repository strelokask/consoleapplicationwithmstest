﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationWithMSTest
{
    public class Game
    {
        public int[,] Board{ get; set; }
        int n, m;

        public Game(int n=10, int m = 10)
        {
            this.n = n;
            this.m = m;

            Board = new int[n, m];

            Initialize();
        }

        internal void NextStep()
        {
            int[,] temp = new int[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    temp[i, j] = GetState(Board[i, j], CountAliveCells(Board, i, j));
                }
            }

            Board = temp;
        }

        public bool HasAliveCell(int[,] array = null)
        {
            if (array != null)
            {
                for (int i = 0; i < array.GetLength(0); i++)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        if (array[i, j] == 1) return true;
                    }
                }
                return false;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (Board[i, j] == 1) return true;
                }
            }
            return false;
        }

        public int CountAliveCells(int[,] array, int x, int y)
        {
            int ans = 0;

            for (int i = x-1; i <= x+1; i++)
            {
                for (int j = y-1; j <= y+1; j++)
                {
                    if (IsCellInRange(i, j, array.GetLength(0), array.GetLength(1)))
                    {
                        if (i == x && j == y) continue;
                        ans += array[i, j];
                    }
                }
            }

            return ans;
        }

        public void NextStepTest(ref int[,] array)
        {

            int n = array.GetLength(0);
            int m = array.GetLength(1);
            int[,] temp = new int[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    temp[i, j] = GetState(array[i, j], CountAliveCells(array, i, j));
                }
            }

            array = temp;
        }


        #region Helper
        private int GetState(int cell, int countNeigboars)
        {
            return countNeigboars == 3 || (cell == 1 && countNeigboars == 2) ? 1 : 0;
        }
        
        private bool IsCellInRange(int i, int j, int n, int m)
        {
            return i >= 0 && j >= 0 && i < n && j < m;
        }

        private void Initialize()
        {
            var random = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Board[i, j] = random.Next(2);
                }
            }
        }
        #endregion
    }
}
