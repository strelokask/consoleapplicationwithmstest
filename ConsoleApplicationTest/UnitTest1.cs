using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplicationWithMSTest;
using System;

namespace ConsoleApplicationTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_Check_IsGameArea_IsNotNull()
        {
            Game game = new Game();

            Assert.IsNotNull(game.Board);
        }

        [TestMethod]
        public void TestMethod_Check_InGameArea_DoesExistAliveCell()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 1 } };

            Assert.IsTrue(game.HasAliveCell(array));
        }

        [TestMethod]
        public void TestMethod_Check_InGameArea_DoesExistAliveCell2()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

            Assert.IsFalse(game.HasAliveCell(array));
        }

        [TestMethod]
        public void TestMethod_Check_CountAliveNeighboarsInXY()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            Assert.IsTrue(game.CountAliveCells(array, 0, 1) == 3);
        }

        [TestMethod]
        public void TestMethod_Check_CountAliveNeighboarsInXY2()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            Assert.IsTrue(game.CountAliveCells(array, 0, 1) == 3);
        }

        [TestMethod]
        public void TestMethod_Check_CountAliveNeighboarsInXY3()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            Assert.IsFalse(game.CountAliveCells(array, 1, 1) == 3);
        }

        [TestMethod]
        public void TestMethod_Check_IsChangeStateCells()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            game.NextStepTest(ref array);

            int[,] array2 = new int[3, 3] { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } };

            Assert.IsTrue(CompareArrays(array, array2));
        }


        [TestMethod]
        public void TestMethod_Check_IsChangeStateCells2()
        {
            Game game = new Game();

            int[,] array = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            game.NextStepTest(ref array);

            int[,] array2 = new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } };

            Assert.IsFalse(CompareArrays(array, array2));
        }

        private bool CompareArrays(int[,] array, int[,] array2)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] != array2[i, j]) return false;
                }
            }
            return true;
        }

    }
}
